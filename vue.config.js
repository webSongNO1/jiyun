const { defineConfig } = require("@vue/cli-service");
const TerserPlugin = require("terser-webpack-plugin");
// 这个文件给vue脚手架用的，脚手架使用webpack封装
module.exports = defineConfig({
  // 打包的时候加载资源从相对路径加载
  publicPath: "./",
  transpileDependencies: false,
  // 关闭源码映射：只在生产环境中：npm run build
  productionSourceMap: false,
  devServer: {
    open: true,
    host: "localhost",
  },
  // vue2脚手架更新后的最后的一个新语法
  configureWebpack: (config) => {
    // 只有npm run build的时候 才是production
    if (process.env.NODE_ENV === "production") {
      return {
        plugins: [
          //打包环境去掉console.log
          new TerserPlugin({
            // 多进程：加速打包速度
            parallel: true,
            terserOptions: {
              ecma: undefined,
              warnings: false,
              parse: {},
              compress: {
                drop_console: true,
                drop_debugger: false,
                pure_funcs: ["console.log"], // 移除console
              },
            },
          }),
        ],
      };
    }
    // cdn托管
    return {
      externals: {
        // key 是index.html cdn链接中暴露出来的顶级对象名字 value 是我们在项目中使用的名字
        vue: "Vue",
        "vue-router": "VueRouter",
        vuex: "Vuex",
        axios: "axios",
      },
    };
  },
});
