// import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
// 全局安装vant
import Vant from "vant";
// import "vant/lib/index.css";
// vant里的按需加载图片的插件需要单独的安装一次
import { Lazyload } from "vant";
Vue.use(Lazyload);

Vue.use(Vant);

Vue.config.productionTip = false;

// 来一个全局的自定义指令，用来权限管理 v-rule
Vue.directive("rule", {
  // 指令的生命周期 inserted 元素加载完毕，只会执行一次
  inserted(el, { value }) {
    el.addEventListener("click", () => {
      // 判断：当点击这个元素的时候，判断是否登录了，登录了就执行value的任务，否则跳转到登录页面
      if (store.getters.IS_LOGIN && value.type === "route") {
        router.push(value.path);
      } else if (store.getters.IS_LOGIN && value.type === "event") {
        value.action();
      } else {
        // TODO:没有登录 这里需要在本地存储一下我们当前的地址，为了将来登录后好跳回来
        store.commit("SET_PATH", router.currentRoute.path);
        router.push("/login");
      }
    });
  },
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
