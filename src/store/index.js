// import Vue from "vue";
// import Vuex from "vuex";
import { login_api } from "@/api/user_api";
import createPersistedState from "vuex-persistedstate";
Vue.use(Vuex);
// 常量的命名规则：全部大写字母
export default new Vuex.Store({
  state: {
    userInfo: {
      id: "", // 用户的id
      islogin: -1, // 1 登录状态 其他就是非登录状态
      username: "",
      token: "", // 身份验证的标识
      host: "", // 用来给项目中图片做基础地址
    },
    // 保存的跳转地址
    originPath: "/",
  },
  getters: {
    IS_LOGIN(state) {
      return state.userInfo.islogin === 1;
    },
    UID(state) {
      return state.userInfo.id;
    },
    USERNAME(state) {
      return state.userInfo.username;
    },
    TOKEN(state) {
      return state.userInfo.token;
    },
  },
  mutations: {
    SET_PATH(state, path) {
      state.originPath = path;
    },
    LOGIN_MUTATION(state, data) {
      state.userInfo = data;
    },
    SET_TOKEN(state, token) {
      state.userInfo.token = token;
    },
  },
  actions: {
    // actions 天生带有promise的属性
    LOGIN_Action({ commit }, params) {
      // TODO: 登录接口写这里
      return login_api(params).then((res) => {
        if (res.data.code === 1) {
          var { host, token, user } = res.data.result;
          commit("LOGIN_MUTATION", { host, token, ...user });
        }
        return res;
      });
      //
    },
  },
  plugins: [
    createPersistedState({
      // 指定一个存储位置
      storage: window.sessionStorage,
    }),
  ],
});
