// import Vue from "vue";
// import VueRouter from "vue-router";
import Home from "@/views/Home";
import store from "@/store";

Vue.use(VueRouter);
// 路由配置数组
const routes = [
  // 页面默认打开的是首页
  {
    path: "/home",
    // 别名
    // alias: "/",
    component: Home,
  },
  // 其他的页面是需要用户手动点击才会显示的，所以为了优化性能，使用路由懒加载
  {
    path: "/search",
    component: () => import("@/views/Search"),
  },
  {
    path: "/shop",
    meta: {
      showTabBar: false,
      auth: true,
    },
    component: () => import("@/views/Shopping"),
  },
  {
    path: "/my",
    meta: {
      auth: true,
    },
    component: () => import("@/views/My"),
  },
  {
    path: "/login",
    meta: {
      auth: false,
      showTabBar: false,
    },
    component: () => import("@/views/Login"),
  },
  {
    path: "/reg",
    meta: {
      showTabBar: false,
      auth: false,
    },
    component: () => import("@/views/Register"),
  },
  {
    path: "/detail/:goods_id", // 动态路由
    props: true, // 当有这个以后，把动态路由的参数给组件的props接受了
    // 路由的元信息
    meta: {
      showTabBar: false,
    },
    component: () => import("@/views/Detail"),
  },
  {
    path: "/server",
    meta: {
      title: "客服",
      auth: true,
      showTabBar: false,
    },
    component: () => import("@/views/Service"),
  },
  {
    path: "/order",
    meta: {
      title: "订单管理",
      auth: true,
      showTabBar: false,
    },
    component: () => import("@/views/Order"),
  },
  {
    path: "/",
    // 重定向：当访问/的时候，自动跳转到/home
    redirect: "/home",
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});
// vue2的router在重定向的时候可能会报错，我们可以通过重写push方法就可以解决
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};
// 使用路由守卫做权限管理 TODO: 控制路由的跳转
router.beforeEach((to, from, next) => {
  // to 要去的路由 from 是当前要离开的路由
  // to.meta.auth 为true就必须判断要登录
  /* 
  1. 需要登录的路由，如果登录了就可以跳转，否认就阻止跳转
  2. 登录后不能进入的路由，如果没有登录就可以跳转，如果登录了就阻止跳转
  3. 普通的路由，无所谓你登录不登录我都可以进入
  */
  if (to.meta.auth === true && store.getters.IS_LOGIN) {
    next();
    // 有些页面如果你登陆了就不能看了：登录和注册
  } else if (to.meta.auth === true && !store.getters.IS_LOGIN) {
    next("/login");
  } else if (to.meta.auth === false && store.getters.IS_LOGIN) {
    next("/");
  } else {
    next();
  }
});

export default router;
