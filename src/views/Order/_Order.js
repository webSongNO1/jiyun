// 混入就是把vue里的某些js功能抽离出来，做到代码的复用以及给组件瘦身
import NavBar from "@/components/NavBar";
export default {
  components: { NavBar },
  data() {
    return {
      active: 2,
      list: [],
    };
  },
  mounted() {
    this.getOrder();
  },
  // 计算属性：可以监听多个值
  computed: {
    com_list() {
      if (this.active === 5) return this.list;
      if (this.active === 6)
        return this.list.filter((item) => {
          return item.state === 5;
        });
      return this.list.filter((item) => item.state === this.active);
    },
  },
  filters: {
    // 过滤器不能用在指令上
    showState(list, active) {
      // 过滤器没有this
      return list.filter((item) => item.state === active);
    },
  },
};
