import Axios from "./base";

// 请求轮播
export const getSild = () => Axios.get("");

// 请求产品列表数据
export const getProductList = (page = 1, number = 10) =>
  Axios.get("/rl", {
    // get方式入参需要写在params里
    params: {
      page,
      number,
    },
  });
