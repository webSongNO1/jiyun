import Axios from "./base";

// 获取订单数据 type类型可选的，获取全部是all
export const order_get_api = (type) =>
  Axios.get("/bill", {
    params: {
      type,
    },
  });
// 操作订单
export const order_set_api = (params) => Axios.post("/bill", params);
