import Axios from "./base";

// 注册
export const reg_api = (user) => Axios.post("/user/registry", user);
// 登录：登录之后后端返回token和用户信息，我们存储到本地（vuex）
export const login_api = (user) => Axios.post("/user", user);
