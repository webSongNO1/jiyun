import Axios from "./base";
// 添加到购物车
export const shop_add_api = (params) => Axios.post("/shop", params);
// 获取购物车 restful 是一种接口设计模式【后端的东西】，post添加 get获取 put更新 delete删除 patch head
export const shop_get_api = () => Axios.get("/shop");
// 删除购物车的某条或多条数据(
export const shop_del_api = (list) =>
  Axios.delete("/shop", {
    data: {
      list,
    },
  });
// 更新购物车的数据-更新数量
export const shop_put_api = (params) => Axios.put("/shop", params);
// 结算购物车
export const shop_pay_api = (list) =>
  Axios.put("/shop/pay", {
    list,
  });
