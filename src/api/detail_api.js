import Axios from "./base";

// 获取详情页的数据
export const getDetail = (goods_id) =>
  Axios.get("/rl/detail", { params: { id: goods_id } });
