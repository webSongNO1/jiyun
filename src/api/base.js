// 封装axios 底层是promise+ajax封装的
// import axios from "axios";
import store from "@/store/index";
import router from "@/router/index";
import { Toast } from "vant";
// 封装一个刷新token的ajax
const flush = axios.create({
  baseURL: "http://websong.wang:4000",
});
flush.interceptors.request.use((config) => {
  // 添加token，必须要有旧的token才能请求到新的token
  config.headers.token = store.getters.TOKEN;
  return config;
});

// 创建一个独立的请求
const Axios = axios.create({
  baseURL: "http://websong.wang:4000",
});

// 请求拦截：除了loading以后就是token
Axios.interceptors.request.use((config) => {
  // 添加token字段
  config.headers.token = store.getters.TOKEN;
  return config;
});

// 响应拦截：除了关闭loading动画，容错，判断数据类型，格式化数据，根据状态码统一管理
Axios.interceptors.response.use((config) => {
  if (config.status === 200 && config.data.code === 1) {
    // 请求成功后，如果用户是登录状态，我们偷偷的刷新token，更新token，保持心跳。
    if (store.getters.IS_LOGIN) {
      flush
        .get("/user/token", {
          params: {
            username: store.getters.USERNAME,
            uid: store.getters.UID,
            islogin: store.getters.IS_LOGIN ? 1 : 0,
          },
        })
        .then((res) => {
          if (res.status === 200 && res.data.code === 1) {
            store.commit("SET_TOKEN", res.data.result.token);
          } else {
            Toast.fail("刷新token失败！");
          }
        });
    }
    return config;
  } else {
    // 如果是因为token过期了，就自动跳到登录页
    if (config.data.code === -100) {
      Toast.fail("请求失败！" + config.data.msg);
      // 还要把本地的vuex的仓库信息重置
      store.commit("LOGIN_MUTATION", {});
      router.replace("/login");
    }
    return Promise.reject(Error("请求失败：" + config.data.msg));
  }
});

export default Axios;
